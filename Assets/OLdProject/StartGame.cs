﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {
	UnityEngine.Video.VideoPlayer videoPlayer;

	// Use this for initialization
	void Start () {
		videoPlayer = GameObject.Find ("Main Camera").GetComponent<UnityEngine.Video.VideoPlayer> ();
		videoPlayer.loopPointReached += EndReached;
	}
	
	// Update is called once per frame
	void Update () {


	}
	void EndReached(UnityEngine.Video.VideoPlayer vp){
		Debug.Log ("load");
		vp.targetCameraAlpha = 0F;
		SceneManager.LoadScene ("Final");
		//AudioSource audio = GameObject.Find ("sound").GetComponent<AudioSource> ();
		//audio.PlayOneShot (Missioncompleted, 1F);
	}
}
