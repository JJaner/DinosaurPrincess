﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class End : MonoBehaviour {
	UnityEngine.Video.VideoPlayer videoPlayer;
	// Use this for initialization

	public AudioClip Missioncompleted;

	void Start () {
		 videoPlayer = GameObject.Find ("Main Camera").GetComponent<UnityEngine.Video.VideoPlayer> ();
		videoPlayer.loopPointReached += EndReached;
	}
	
	// Update is called once per frame
	void Update () {		
}
	void EndReached(UnityEngine.Video.VideoPlayer vp){
		Debug.Log ("STOP FERTIG");
		vp.targetCameraAlpha = 0F;
		SceneManager.LoadScene ("Main Menu");
		//AudioSource audio = GameObject.Find ("sound").GetComponent<AudioSource> ();
		//audio.PlayOneShot (Missioncompleted, 1F);
	}
}
