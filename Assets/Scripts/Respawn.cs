﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Respawn : MonoBehaviour {

	// Use this for initialization
	public AudioClip death;
	void Start () {
		deathText.text = "Deaths: ";
	}
	CharacterCollision characterCollision;
	
	// Update is called once per frame
	void Update () {
		characterCollision = GameObject.FindGameObjectWithTag("character").GetComponent<CharacterCollision>();
		savePoint =  characterCollision.SavePoint();
	}

	public int DeathCount = 0;
	public Text deathText;
	Vector3 savePoint;
	
	 void OnTriggerEnter(Collider col) {
	if(col.gameObject.CompareTag("character"))
	{
		DeathCount +=1;
		deathText.text = "Deaths: "+ DeathCount;

		characterCollision.Death();
		AudioSource audio = GameObject.Find ("Sound Death").GetComponent<AudioSource> ();
			audio.PlayOneShot (death, 0.5F);
		characterCollision.respawn();
		
	}
         
     }
}
