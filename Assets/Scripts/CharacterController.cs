﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

	
	public float maxSpeed = 7;
	public float moveSpeed = 5;
	public float defaultMoveSpeed;
	
	
	Vector3 direction = new Vector3(1,0,0);
	[SerializeField]Rigidbody rigidbody;

	[Header("Jumping")]

   // [SerializeField] private float m_height = 10f;
	[SerializeField] private float JumpSpeedMultiplier = 10f;
	public float maxHeight= 5;
	 public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
	public float maxTimeOfJumpUp = 0.5f;
	float TimeOfInitialJump;
	public AudioClip Jumpsound;

	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
		rigidbody = GetComponent<Rigidbody>();

		defaultMoveSpeed = moveSpeed;
	}
	
	// Update is called once per frame
	void Update () {

		
		Jump();
	}

	void FixedUpdate()
	{
		Move();	
		
		
	}


	void Jump() {

	
		if(rigidbody.velocity.y < 0)
        {
            rigidbody.velocity += (new Vector3(rigidbody.velocity.x, 0,rigidbody.velocity.z) + Vector3.up )*Physics.gravity.y * (fallMultiplier -1) * Time.deltaTime;

        }else if(rigidbody.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rigidbody.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier -1) * Time.deltaTime;
        }


		if(Input.GetButtonDown("Jump") && m_isGrounded)
		{
			print("Jump");
				AudioSource audio = GameObject.Find ("JumpSound").GetComponent<AudioSource> ();
		audio.PlayOneShot (Jumpsound, 0.5F);
			rigidbody.velocity = new Vector3(0,maxHeight,0);
			 TimeOfInitialJump = Time.time;
		}

		if(Input.GetButton("Jump")&& !m_isGrounded)
		{
			if(Time.time - TimeOfInitialJump <= maxTimeOfJumpUp)
			{
				
				Vector3 vel =  rigidbody.velocity;
				vel.y-=JumpSpeedMultiplier*Time.deltaTime*Time.deltaTime;
				rigidbody.velocity=vel + new Vector3(0,Time.deltaTime ,0);
	
			}else
			{
				rigidbody.velocity += Vector3.up *Physics.gravity.y * (fallMultiplier -1) * Time.deltaTime;
		
			}

		}

		if(Input.GetButtonUp("Jump"))
		{
		
		}
	}

	float MoveMagnitude;

	 bool isCharacterFacingLeft = false;
	void Move(){
		float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

		

		if(Input.GetKey(KeyCode.D))
		{
			print("Right");
	
		
			this.transform.position += direction * moveSpeed * Time.deltaTime;
			//rigidbody.velocity = new Vector3(moveSpeed,0,0);
			
		}
		else if(Input.GetKey(KeyCode.A))
		{
			print("Left");
			
			transform.position -= direction * moveSpeed * Time.deltaTime;

			//rigidbody.velocity = new Vector3(-1*moveSpeed,0,0);
		}

		if(Input.GetKeyDown(KeyCode.A))
		{
			if(!isCharacterFacingLeft)
			{
				transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * -1);
				isCharacterFacingLeft = true;
			}
				
		}
		if(Input.GetKeyDown(KeyCode.D))
		{
			if(isCharacterFacingLeft)
			{
				transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z * -1);
				isCharacterFacingLeft = false;
			}
			
		}
	}


	// Ground Collision Checker
	 private bool m_wasGrounded;
	 private bool m_isGrounded;

    private List<Collider> m_collisions = new List<Collider>();

	private void OnCollisionEnter(Collision collision)
    {
		print(collision.gameObject.name);
        ContactPoint[] contactPoints = collision.contacts;
        for(int i = 0; i < contactPoints.Length; i++)
        {	

			/*if(Vector3.Dot(contactPoints[i].normal, transform.forward) > 0)
			{
				print("pushing against an object");
				transform.position = transform.position;
				Vector3 d = -(contactPoints[i].normal - transform.forward).normalized;
				rigidbody.AddForce(d*MoveMagnitude);
			}*/
            if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0f)
            {
                if (!m_collisions.Contains(collision.collider)) {
                    m_collisions.Add(collision.collider);
                }
                m_isGrounded = true;
            }
		

			
        }
		/*string name = collision.gameObject.name;
		if(name == "1"||name == "4(1)"||name == "5"|| name == "3"||name == "2"||name == "1(1)"||name == "2(1)"||name == "2(2)"||name == "1(2)")
		{ 
			m_moveSpeed =defaultMoveSpeed * 0.3f;
			
			isOnLilyPad = true;
		}*/
    }
	//bool isOnLilyPad;
	string previous_colname;
    private void OnCollisionStay(Collision collision)
    {
        ContactPoint[] contactPoints = collision.contacts;
        bool validSurfaceNormal = false;
		
        for (int i = 0; i < contactPoints.Length; i++)
        {
			
			
			previous_colname = collision.gameObject.name;
            if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0f)
            {
                validSurfaceNormal = true; break;
            }

			
        }

        if(validSurfaceNormal)
        {
            m_isGrounded = true;
            if (!m_collisions.Contains(collision.collider))
            {
                m_collisions.Add(collision.collider);
            }
        } else
        {
            if (m_collisions.Contains(collision.collider))
            {
                m_collisions.Remove(collision.collider);
            }
            if (m_collisions.Count == 0) { m_isGrounded = false; }
        }
    }

    private void OnCollisionExit(Collision collision)
    {

		/*if(isOnLilyPad)
		{
			m_moveSpeed = defaultMoveSpeed;
			//isOnLilyPad = false;
		}*/
        if(m_collisions.Contains(collision.collider))
        {
            m_collisions.Remove(collision.collider);
        }
        if (m_collisions.Count == 0) { m_isGrounded = false; }
    }
    

}
