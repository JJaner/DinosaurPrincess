﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CharacterCollision : MonoBehaviour {

	// Use this for initialization

	public Vector3 respawnPoint;
	public Text meatCount;
	public int count =0;
	public AudioClip[] Meat;
	private AudioClip MeatGet;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		meatCount.text = "x "+ count;
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("meat"))
		{
			print("character took the meat");
			AudioSource audio = GameObject.Find ("Sound Death").GetComponent<AudioSource> ();
			int index = Random.Range(0,Meat.Length);
			MeatGet = Meat[index];
			audio.PlayOneShot (MeatGet, 1F);
			count++;
			Destroy(other.gameObject);
		}

		if(other.gameObject.CompareTag("Respawn"))
		{
			respawnPoint = other.transform.position;

		}

		if(other.gameObject.CompareTag("Finish"))
		{
			SceneManager.LoadScene("End");

		}
	}

     public Transform GameOverScreen;
	public void Death()
	{
		if(count >= 5)
		count -= 5;
		else if(count < 5)
		{
		
			if(count == 0)
			{
				Time.timeScale = 0;
				GameOverScreen.gameObject.SetActive(true);
			}
				count = 0;
		}
		
	}
	public void respawn()
	{
		transform.position = respawnPoint;
	}

	public Vector3 SavePoint()
	{
		return respawnPoint;
	}
}
