﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

	public Transform target;
	public float smoothSpeed = 0.125f;
	public Vector3 offset;

	float y;
	void Start () {
		y = target.position.y;
	}
		bool firstRun= true;
Vector3 desiredPosition;
	void FixedUpdate () {

		if(target.position.y < 3 && firstRun)
		{
			desiredPosition = new Vector3(target.position.x, y, target.position.z)+offset;
			Vector3 smoothPosition = Vector3.Lerp(new Vector3(transform.position.x, y, transform.position.z), desiredPosition,smoothSpeed);
			transform.position = smoothPosition;
			

		} else if(target.position.y > 3 && firstRun){
			firstRun = false;
			desiredPosition = target.position+new Vector3 (offset.x, 0, offset.z);
			Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition,smoothSpeed);
			transform.position = smoothPosition;

		}else if(target.position.y > 3 && !firstRun)
		{
			desiredPosition = target.position+new Vector3 (offset.x, 0, offset.z);
			Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition,smoothSpeed);
			transform.position = smoothPosition;
		}else if(target.position.y < 3 && !firstRun)
		{
			desiredPosition = target.position+new Vector3 (offset.x, y, offset.z);
			Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition,smoothSpeed);
			transform.position = smoothPosition;
		}
		
		//transform.LookAt(target);
	}

	public void FollowTarget()
	{

	}
}
