﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

    public float timeLeft = 60.0f;
     
     public Text text;

     public Transform GameOverScreen;
     
 
     
     void Update()
     {
         timeLeft -= Time.deltaTime;
         text.text = " " + Mathf.Round(timeLeft);
         if(timeLeft < 0)
         {
             Time.timeScale = 0;
             GameOverScreen.gameObject.SetActive(true);
             print("GameOver");
         }
     }

     public void Restart()
     {
         
         SceneManager.LoadScene(2);
     }
     public void Quit()
     {
         SceneManager.LoadScene(0);
     }
}
