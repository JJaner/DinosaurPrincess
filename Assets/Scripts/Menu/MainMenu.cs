﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class MainMenu : MonoBehaviour {

	public GameObject loadingScreen;
	public Slider Slider;
	public AudioClip Buttonssound;

	void Start()
	{
		
		Time.timeScale = 1;
	}

	public void PlayGame()
	{
		AudioSource audio = GameObject.Find ("ButtonSound").GetComponent<AudioSource> ();
		audio.PlayOneShot (Buttonssound, 0.5F);
		LoadLevel(1);
	}

	public void Credit()
	{
		AudioSource audio = GameObject.Find ("ButtonSound").GetComponent<AudioSource> ();
		audio.PlayOneShot (Buttonssound, 0.5F);
		LoadLevel(4);
	}
	public void Back()
	{
		AudioSource audio = GameObject.Find ("ButtonSound").GetComponent<AudioSource> ();
		audio.PlayOneShot (Buttonssound, 0.5F);
		LoadLevel(0);
	}


	

	public void QuitGame(){
		AudioSource audio = GameObject.Find ("ButtonSound").GetComponent<AudioSource> ();
		audio.PlayOneShot (Buttonssound, 0.5F);
		Application.Quit();
			print("QuitGame!");
	}


	public void LoadLevel(int sceneIndex)
	{
		StartCoroutine(LoadAsynchronously(sceneIndex));
	}

	IEnumerator LoadAsynchronously(int sceneIndex)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
		loadingScreen.SetActive(true);
		if(sceneIndex != 1)
		{
			loadingScreen.transform.GetChild(1).transform.GetChild(Random.Range(0,6)).gameObject.SetActive(true);
		}else{
			loadingScreen.transform.GetChild(0).gameObject.SetActive(true);
		}
		while(!operation.isDone)
		{
			float progress = Mathf.Clamp01(operation.progress/.9f);
			Debug.Log(progress);
			Slider.value = progress;

			yield return null;
		}
	}
}
