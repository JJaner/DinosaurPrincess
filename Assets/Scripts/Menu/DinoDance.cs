﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoDance : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	float interval = 0.5f;
	float nextTime = 0;
	bool toRight = false;
	// Update is called once per frame
	void Update () {

		if(Time.time >= nextTime)
		{
			if(toRight)
			{
				this.GetComponent<RectTransform>().Rotate(new Vector3 (0,0,5));
				toRight = false;
			}else{
				this.GetComponent<RectTransform>().Rotate(new Vector3 (0,0,-5));
				toRight = true;
			}
				
			
			nextTime += interval;
		}
		
	}
}
